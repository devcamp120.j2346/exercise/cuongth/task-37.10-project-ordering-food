import React from "react";
import "./style.css";

export const Frame = () => {
  return (
    <div className="frame">
      <div className="navbar">
        <div className="text-wrapper">Home</div>
        <div className="text-wrapper">About</div>
        <div className="text-wrapper">Menu</div>
        <div className="text-wrapper">Blog</div>
        <div className="text-wrapper">Contact</div>
      </div>
      <img className="seach-and-cart" alt="Seach and cart" src="/img/seach-and-cart.svg" />
    </div>
  );
};
